﻿// My Script
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MapGenerator : MonoBehaviour {

	public int width;
	public int height;

	public string seed;
	public bool useRandomSeed;

	[Range(0,100)]
	public int randomFillPercent; 

	int[,] map;

	// we set up a random config

	void Start(){
		GenerateMap();
	}

	void Update(){
		if (Input.GetMouseButtonDown (0)) {
			GenerateMap ();
		}
	}

	void GenerateMap(){
		map = new int[width , height];
		RandomFillMap ();

		for (int i = 0; i < 5; i++) {
			SmoothMap ();
		}

		MeshGenerator meshGen = GetComponent<MeshGenerator>();
		meshGen.GenerateMesh(map, 1);
	}

	void RandomFillMap(){
		// when useRandomSeed is checked produces the same map
		if (useRandomSeed) {
			string seed = string.Format("{0:HH:mm:ss tt}", DateTime.Now);
			//seed = Time.time.ToString ();
			print(seed);
			print(seed.GetHashCode());
		} 

		System.Random pseudoRandom = new System.Random (seed.GetHashCode ());
		bool flag = true;
		int counter = 0;
		int var;
		while (flag){
			var = pseudoRandom.Next(0, 100);
			counter++;
			if (counter > 10000 || var > 50){
				flag = false;
			}
		}
		print(pseudoRandom.Next(100));
		print(counter);
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				// this ensures we have walls on the bordor of the map
				if (x == 0 || x == width - 1 || y == 0 || y == height - 1) {
					map [x, y] = 1;
				} 
				else {
					map [x, y] = (pseudoRandom.Next (0, 100) < randomFillPercent) ? 1 : 0;
				}
			}
		}
	}


	void SmoothMap(){
	// counts the number of neighboring walls of every cell
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				int neighbourWallTiles = GetSurroundingWallCount (x, y);

				if (4 < neighbourWallTiles) {
					map [x, y] = 1;
				} else if (neighbourWallTiles <= 4) {
					map [x, y] = 0;
				}
			}
		}
	}

	int GetSurroundingWallCount(int gridX, int gridY){
		int wallCount = 0;
		for (int neighbourX = gridX - 1; neighbourX <= gridX + 1; neighbourX++) {
			for (int neighbourY = gridY - 1; neighbourY <= gridY + 1; neighbourY++) {
				if (0 <= neighbourX && neighbourX < width && 0 <= neighbourY && neighbourY < height) { 
					if (neighbourX != gridX || neighbourY != gridY) {
						wallCount += map [neighbourX, neighbourY];
					}
				} 
				else{
					wallCount += 1;
				}
			}
		}
		return wallCount;
	}

	void OnDrawGizmos(){
		/*
		if (map != null) {
			for (int x = 0; x < width; x++) {
				for (int y = 0; y < height; y++) {
					Gizmos.color = (map [x, y] == 1) ? Color.black : Color.white;
					Vector3 pos = new Vector3 (-width + x + 0.5f, -height + y + 0.5f);
					Gizmos.DrawCube (pos, Vector3.one);
				}
			}
		}
		*/
	}
}
/*
// Episode 1
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MapGenerator : MonoBehaviour {

	public int width;
	public int height;

	public string seed;
	public bool useRandomSeed;

	[Range(0,100)]
	public int randomFillPercent; 

	int[,] map;

	// we set up a random config

	void Start(){
		GenerateMap();
	}

	void Update(){
		if (Input.GetMouseButtonDown (0)) {
			GenerateMap ();
		}
	}

	void GenerateMap(){
		map = new int[width , height];
		RandomFillMap ();

		for (int i = 0; i < 5; i++) {
			SmoothMap ();
		}
	}

	void RandomFillMap(){
		// when useRandomSeed is checked produces the same map
		if (useRandomSeed) {
			string seed = string.Format("{0:HH:mm:ss tt}", DateTime.Now);
			//seed = Time.time.ToString ();
			print(seed);
			print(seed.GetHashCode());
		} 

		System.Random pseudoRandom = new System.Random (seed.GetHashCode ());
		bool flag = true;
		int counter = 0;
		int var;
		while (flag){
			var = pseudoRandom.Next(0, 100);
			counter++;
			if (counter > 10000 || var > 50){
				flag = false;
			}
		}
		print(pseudoRandom.Next(100));
		print(counter);
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				// this ensures we have walls on the bordor of the map
				if (x == 0 || x == width - 1 || y == 0 || y == height - 1) {
					map [x, y] = 1;
				} 
				else {
					map [x, y] = (pseudoRandom.Next (0, 100) < randomFillPercent) ? 1 : 0;
				}
			}
		}
	}


	void SmoothMap(){
	// counts the number of neighboring walls of every cell
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				int neighbourWallTiles = GetSurroundingWallCount (x, y);

				if (4 < neighbourWallTiles) {
					map [x, y] = 1;
				} else if (neighbourWallTiles <= 4) {
					map [x, y] = 0;
				}
			}
		}
	}

	int GetSurroundingWallCount(int gridX, int gridY){
		int wallCount = 0;
		for (int neighbourX = gridX - 1; neighbourX <= gridX + 1; neighbourX++) {
			for (int neighbourY = gridY - 1; neighbourY <= gridY + 1; neighbourY++) {
				if (0 <= neighbourX && neighbourX < width && 0 <= neighbourY && neighbourY < height) { 
					if (neighbourX != gridX || neighbourY != gridY) {
						wallCount += map [neighbourX, neighbourY];
					}
				} 
				else{
					wallCount += 1;
				}
			}
		}
		return wallCount;
	}

	void OnDrawGizmos(){
		if (map != null) {
			for (int x = 0; x < width; x++) {
				for (int y = 0; y < height; y++) {
					Gizmos.color = (map [x, y] == 1) ? Color.black : Color.white;
					Vector3 pos = new Vector3 (-width + x + 0.5f, -height + y + 0.5f);
					Gizmos.DrawCube (pos, Vector3.one);
				}
			}
		}
	}
}
 */