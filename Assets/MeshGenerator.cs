﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshGenerator : MonoBehaviour {
	public SquareGrid squareGrid;
	List<Vector3> vertices;
	List<int> triangles;

	public void GenerateMesh(int[,] map, float squareSize){
		squareGrid = new SquareGrid(map, squareSize);

		vertices = new List<Vector3>();
		triangles = new List<int>();

		for (int x = 0; x < squareGrid.squares.GetLength(0); x++){
			for (int y = 0; y < squareGrid.squares.GetLength(1); y++){
				TriangulateSquare(squareGrid[x,y]);
			}
		}
		Mesh mesh = new Mesh();
		GetComponent<MeshFilter>().mesh = mesh;

		mesh.vertices = vertices.ToArray();
		mesh.triangles = triangles.ToArray();
		mesh.RecalculateNormals();
	}

	void TriangulateSquare(Square square){
		switch(square.configuration){
			case 0:
				break;
			
			// 1 point is selected
			case 1:
				MeshFromPoints(square.centerBottom, square.bottomLeft, square.centerLeft);
				break;
			case 2:
				MeshFromPoints(square.centerRight, square.bottomRight, square.centerBottom);
				break;
			case 4:
				MeshFromPoints(square.centerTop, square.topRight, square.centerRight);
				break;
			case 8:
				MeshFromPoints(square.topLeft, square.centerTop, square.centerLeft);
				break;

			// 2 points are selected
			case 3:
				MeshFromPoints(square.centerRight, square.bottomRight, square.bottomLeft, square.centerLeft);
				break;
			case 6:
				MeshFromPoints(square.centerTop, square.topRight, square.bottomRight, square.centerBottom);
				break;
			case 9:
				MeshFromPoints(square.topLeft, square.centerTop, square.centerBottom, square.bottomLeft);
				break;
			case 12:
				MeshFromPoints(square.topLeft, square.topRight, square.centerRight, square.centerLeft);
				break;
			case 5:
				MeshFromPoints(square.centerTop, square.topRight, square.centerRight, square.centerBottom, square.bottomLeft, square.centerLeft);
				break;
			case 10:
				MeshFromPoints(square.topLeft, square.centerTop, square.centerRight, square.bottomRight, square.centerBottom, square.centerLeft);
				break;

			// 3 points are selected
			case 7:
				MeshFromPoints(square.centerTop, square.topRight, square.bottomRight, square.bottomLeft, square.centerLeft);
				break;
			case 7:
				MeshFromPoints(square.centerTop, square.topRight, square.bottomLeft, square.bottomRight, square.centerLeft);
				break;
			case 7:
				MeshFromPoints(square.centerTop, square.topRight, square.bottomLeft, square.bottomRight, square.centerLeft);
				break;
			case 7:
				MeshFromPoints(square.centerTop, square.topRight, square.bottomLeft, square.bottomRight, square.centerLeft);
				break;
			
			// 4 points are selected
			case 15:
				MeshFromPoints(square.topLeft, square.topRight, square.bottomRight, square.bottomleft);
				break;	
		}
	}

	// params specifies an unknown number of points.
	void MeshFromPoints(params Node[] nodes){
		AssignVertices(points);

		if (points.Length >= 3)
			CreateTriangle(points[0], points[1], points[2]);
		if (points.Length >= 4)
			CreateTriangle(points[0], points[2], points[3]);
		if (points.Length >= 5)
			CreateTriangle(points[0], points[3], points[4]);
		if (points.Length >= 6)
			CreateTriangle(points[0], points[4], points[5]);

	}

	void AssignVertices(Node[] points){
		for (int i = 0; i < points.Length; i++){
			if (point[i].vertexIndex == -1){ // by default vertexIndex is -1 which indicates it is not assigned yet.
				points[i].vertexIndex = vertices.Count;
				vertices.Add(points[i].position);
			}
		}
	}

	void CreateTriangle(Node a, Node b, Node c){
		triangles.Add(a.vertexIndex);
		triangles.Add(b.vertexIndex);
		triangles.Add(c.vertexIndex);
	}

	void OnDrawGizmos(){
		/*
		if (squareGrid != null){
			for (int x = 0; x < squareGrid.squares.GetLength(0); x++){
				for (int y = 0; y < squareGrid.squares.GetLength(1); y++){
					Gizmos.color = (squareGrid.squares[x,y].topLeft.active)?Color.black:Color.white;
					Gizmos.DrawCube(squareGrid.squares[x,y].topLeft.position, Vector3.one * 0.4f);

					Gizmos.color = (squareGrid.squares[x,y].topRight.active)?Color.black:Color.white;
					Gizmos.DrawCube(squareGrid.squares[x,y].topRight.position, Vector3.one * 0.4f);

					Gizmos.color = (squareGrid.squares[x,y].bottomLeft.active)?Color.black:Color.white;
					Gizmos.DrawCube(squareGrid.squares[x,y].bottomLeft.position, Vector3.one * 0.4f);

					Gizmos.color = (squareGrid.squares[x,y].bottomRight.active)?Color.black:Color.white;
					Gizmos.DrawCube(squareGrid.squares[x,y].bottomRight.position, Vector3.one * 0.4f);

					Gizmos.color = Color.grey;
					Gizmos.DrawCube(squareGrid.squares[x,y].centerTop.position, Vector3.one * 0.15f);
					Gizmos.DrawCube(squareGrid.squares[x,y].centerRight.position, Vector3.one * 0.15f);
					Gizmos.DrawCube(squareGrid.squares[x,y].centerBottom.position, Vector3.one * 0.15f);
					Gizmos.DrawCube(squareGrid.squares[x,y].centerLeft.position, Vector3.one * 0.15f);
				}
			}
		}
		*/
	}

	public class SquareGrid{

		public Square[,] squares;

		public SquareGrid(int[,] map, float squareSize){
			int nodeCountX = map.GetLength(0);
			int nodeCountY = map.GetLength(1);
			float mapWidth = nodeCountX * squareSize;
			float mapHeight = nodeCountY * squareSize;

			ControlNode[,] controlNode = new ControlNode[nodeCountX, nodeCountY];

			for (int x = 0; x < nodeCountX; x++){
				for (int y = 0; y < nodeCountY; y++){
					// calculate pos of current control node
					Vector3 pos = new Vector3(-mapWidth/2 + x * squareSize + squareSize/2, 0, -mapHeight/2 + y * squareSize + squareSize/2);
					controlNode[x, y] = new ControlNode(pos, map[x,y] ==1, squareSize);
				}
			}
			squares = new Square[nodeCountX - 1, nodeCountY - 1];
			for (int x = 0; x < nodeCountX - 1; x++){
				for (int y = 0; y < nodeCountY - 1; y++){
					squares[x,y] = new Square(controlNode[x, y+1], controlNode[x+1,y+1], controlNode[x+1,y], controlNode[x,y]);
				}
			}
		}
	}
	

	public class Square{

		public ControlNode topLeft, topRight, bottomLeft, bottomRight;
		public Node centerTop, centerBottom, centerRight, centerLeft;
		public int configuration;

		// there are 16 configurations
		public Square(ControlNode _topLeft, ControlNode _topRight, ControlNode _bottomLeft, ControlNode _bottomRight){
			topLeft = _topLeft;
			topRight = _topRight;
			bottomLeft = _bottomLeft;
			bottomRight = _bottomRight;

			centerTop = topLeft.right;
			centerRight = bottomRight.above;
			centerBottom = bottomLeft.right; 
			centerLeft = bottomLeft.above;

			// verify if the controls nodes are active
			if (topLeft.active)
				configuration += 8;
			if (topRight.active)
				configuration += 4;
			if (bottomRight.active)
				configuration += 2;
			if (bottomLeft.active)
				configuration += 1;
		}
	}

	public class Node{
		public Vector3 position;
		public int vertexIndex = -1;

		public Node(Vector3 _pos){
			position = _pos;
		}
	}

	// following syntax indicates ControlNode inherits from Node.
	public class ControlNode : Node{
		// true means wall, false means no wall.
		public bool active;
		public Node above, right;

		// since it is inheriting from Node:
		public ControlNode(Vector3 _pos, bool _active, float squareSize):base(_pos){
			active = _active;
			above = new Node(position + Vector3.forward * squareSize/2f);
			right = new Node(position + Vector3.right * squareSize/2f);
		}
	}
}
